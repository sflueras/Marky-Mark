// swift-tools-version:5.0

import PackageDescription

let package = Package(
    name: "Marky-Mark",
    platforms: [
        .iOS("12.0"),
    ],
    products: [
        .library(
            name: "markymark",
            targets: ["markymark"]),
    ],
    dependencies: [
        .package(url: "https://github.com/onevcat/Kingfisher.git", from: "7.0.0")
    ],
    targets: [
        .target(
            name: "markymark",
            dependencies: ["Kingfisher"]
        ),
        .testTarget(
            name: "MarkyMarkTests",
            dependencies: ["markymark"]
        ),
    ],
    swiftLanguageVersions: [.v5]
)
