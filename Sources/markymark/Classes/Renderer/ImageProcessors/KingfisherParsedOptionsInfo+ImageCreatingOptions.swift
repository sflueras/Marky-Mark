//
//  KingfisherParsedOptionsInfo+ImageCreatingOptions.swift
//  
//
//  Created by Stefan Compton on 4/27/22.
//

import Kingfisher

extension KingfisherParsedOptionsInfo {
    var imageCreatingOptions: ImageCreatingOptions {
        return ImageCreatingOptions(
            scale: scaleFactor,
            duration: 0.0,
            preloadAll: preloadAllAnimationData,
            onlyFirstFrame: onlyLoadFirstFrame
        )
    }
}
