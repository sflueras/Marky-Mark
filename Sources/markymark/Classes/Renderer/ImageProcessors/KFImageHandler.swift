//
//  KFImageHandler.swift
//  
//
//  Created by Stefan Compton on 4/29/22.
//

import Foundation
import Kingfisher

final class KFImageHandler: AsyncImageHandler {

    let cacheName: String

    init(cacheName: String? = nil) {
        self.cacheName = cacheName ?? Self.defaultCacheName
    }

    func retrieveImage(with url: URL, completionHandler: @escaping ((Result<AsyncImage, Error>) -> Void)) {
        // This will retrieve the image from the cache or download it (async) and store it in the cache.
        // The completion handler will be invoked on the main thread.
        KingfisherManager.shared.retrieveImage(with: url, options: options) { result in
            let wrappedResult = result
                .map { AsyncImage(image: $0.image) }
                .mapError { $0 as Error }
            completionHandler(wrappedResult)
        }
    }

    // MARK: - Private

    private static let defaultCacheName = "com.m2mobi.marky-mark.ImageCache"

    private lazy var options: KingfisherOptionsInfo = {
        [
            .processor(MarkDownImageProcessor()),
            .targetCache(ImageCache(name: cacheName)),
            .cacheSerializer(MarkDownCacheSerializer())
        ]
    }()
}
