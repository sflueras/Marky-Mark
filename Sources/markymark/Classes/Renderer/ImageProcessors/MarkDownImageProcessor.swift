//
//  MarkDownImageProcessor.swift
//  markymark
//
//  Created by Stefan Compton on 10/14/20.
//

import Kingfisher
import UIKit

struct MarkDownImageProcessor: ImageProcessor {

    // `identifier` should be the same for processors with same properties/functionality
    // It will be used when storing and retrieving the image to/from cache.
    let identifier = "com.m2mobi.marky-mark.MarkDownImageProcessor"

    // Convert input data/image to target image and return it.
    func process(item: ImageProcessItem, options: KingfisherParsedOptionsInfo) -> KFCrossPlatformImage? {
        switch item {
        case .image(let image):
            return image.kf.scaled(to: options.scaleFactor)
        case .data(let data):
            // If we have a known/supported image format then defer to standard data processing
            guard data.kf.imageFormat == .unknown else {
                return KingfisherWrapper.image(data: data, options: options.imageCreatingOptions)
            }

            return PDFConverter.image(with: data)
        }
    }
}
