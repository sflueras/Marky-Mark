//
//  MarkDownCacheSerializer.swift
//  
//
//  Created by Stefan Compton on 4/27/22.
//

import Foundation
import CoreGraphics
import Kingfisher

struct MarkDownCacheSerializer: CacheSerializer {

    /// The compression quality when converting image to a lossy format data. Default is 1.0.
    var compressionQuality: CGFloat = 1.0

    func data(with image: KFCrossPlatformImage, original: Data?) -> Data? {
        guard original?.kf.imageFormat == .unknown else {
            return image.kf.data(
                format: original?.kf.imageFormat ?? .unknown,
                compressionQuality: compressionQuality
            )
        }
        return original
    }

    func image(with data: Data, options: KingfisherParsedOptionsInfo) -> KFCrossPlatformImage? {
        // If we have a known/supported image format then defer to standard data processing
        guard data.kf.imageFormat == .unknown else {
            return KingfisherWrapper.image(data: data, options: options.imageCreatingOptions)
        }

        return PDFConverter.image(with: data)
    }
}
