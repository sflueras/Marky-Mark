//
//  PDFConverter.swift
//  
//
//  Created by Stefan Compton on 4/27/22.
//

import Foundation
import CoreGraphics
import UIKit

struct PDFConverter {

    static func image(with data: Data) -> UIImage? {
        // If the image format is unknown then try PDF
        let pdfData = data as CFData
        guard
            let provider = CGDataProvider(data: pdfData),
            let pdfDoc = CGPDFDocument(provider),
            let pdfPage = pdfDoc.page(at: 1)
            else {
                return nil
        }

        var pageRect = pdfPage.getBoxRect(.mediaBox)
        pageRect.size = CGSize(width: pageRect.size.width, height: pageRect.size.height)
        UIGraphicsBeginImageContextWithOptions(pageRect.size, false, UIScreen.main.scale)
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        context.saveGState()
        context.translateBy(x: 0.0, y: pageRect.size.height)
        context.scaleBy(x: 1, y: -1)
        context.concatenate(
            pdfPage.getDrawingTransform(.mediaBox, rect: pageRect, rotate: 0, preserveAspectRatio: true))
        context.drawPDFPage(pdfPage)
        context.restoreGState()
        let pdfImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return pdfImage
    }
}
