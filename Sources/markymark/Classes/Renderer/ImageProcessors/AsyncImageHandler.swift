//
//  AsyncImageHandler.swift
//  
//
//  Created by Stefan Compton on 4/29/22.
//

import UIKit

protocol AsyncImageHandler {

    /// Name for image cache used by this handler.
    var cacheName: String { get }

    /// Retrieves an image asynchronously.
    /// - Parameters:
    ///   - url: the image URL.
    ///   - completionHandler: called when the image retrieval is finished.
    func retrieveImage(with url: URL, completionHandler: @escaping ((Result<AsyncImage, Error>) -> Void))
}

struct AsyncImage {
    let image: UIImage
}
