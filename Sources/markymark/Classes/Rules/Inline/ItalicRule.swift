//
//  Created by Jim van Zummeren on 04/05/16.
//  Copyright © 2016 M2mobi. All rights reserved.
//

import UIKit

open class ItalicRule: InlineRegexRule {

    public init() {}

    /// Example: *text* or _text_
    /// - (?<!_|\\*|\\\\) Negative lookbehind matching `_` OR `*` OR `\\`
    /// - (_|\\*) 1st Capture group matching `_` OR `*`
    /// - (?!_|\\*) Negative lookahead matching `_` OR `*`
    /// - ((?:[^\\\\_\\*\\n]|\\\\.)+) 2nd Capture group
    ///     - (?:[^\\\\_\\*\\n]|\\\\.)+ Non-capturing group where `+` matches previous token one or unlimited times
    ///         - [^\\\\_\\*\\n] match a single character that is not `\\`, `_`, `*`, or `\n` OR
    ///         - \\\\. match any escaped character
    /// - (_|\\*) 1st Capture group matching `_` OR `*`
    /// - (?!_|\\*) Negative lookahead matching `_` OR `*`
    open var expression = NSRegularExpression.expressionWithPattern("(?<!_|\\*|\\\\)(_|\\*)(?!_|\\*)((?:[^\\\\_\\*\\n]|\\\\.)+)(_|\\*)(?!_|\\*)")

    // MARK: Rule

    open func createMarkDownItemWithLines(_ lines: [String]) -> MarkDownItem {
        let content = lines.first?.subStringWithExpression(expression, ofGroup: 2)
        return ItalicMarkDownItem(lines: lines, content: content ?? "")
    }

}
