//
//  Created by Maren Osnabrug on 10-05-16.
//  Copyright © 2016 M2mobi. All rights reserved.
//

import XCTest
@testable import markymark

class ItalicRuleTests: XCTestCase {

    let italicCharacters: [String] = ["*", "_"]
    var sut: ItalicRule!

    override func setUp() {
        super.setUp()
        sut = ItalicRule()
    }

    func testRecognizesLines() {
        let templateValue: [String: Bool] = [
            "{~}Text that is Italic{~}": true,
            "{~}{~}Text that is bold{~}{~}": false,
            "\\{~}Text that has escaped italics\\{~}": false
        ]
        for (template, value) in templateValue {
            for character in italicCharacters {
                let string = template.replacingOccurrences(of: "{~}", with: character)
                XCTAssertEqual(sut.recognizesLines([string]), value)
            }
        }
    }

    func testCreateMarkDownItemWithLinesCreatesCorrectItem() {
        //Arrange
        let markdownItem = sut.createMarkDownItemWithLines(["*Text*"])
        //Act

        //Assert
        XCTAssert(markdownItem is ItalicMarkDownItem)
    }

    func testCreateMarkDownItemContainsCorrectText() {
        //Arrange
        let templateValue: [String: String] = [
            "{~}Text{~}": "Text",
            "{~}Text that is Italic{~}": "Text that is Italic",
            "{~}\\{~}Text that is Italic with escape char at start{~}": "\\{~}Text that is Italic with escape char at start",
            "{~}Text that is Italic with escape char at end\\{~}{~}": "Text that is Italic with escape char at end\\{~}",
        ]
        //Act

        //Assert
        for (template, value) in templateValue {
            for character in italicCharacters {
                let string = template.replacingOccurrences(of: "{~}", with: character)
                let content = value.replacingOccurrences(of: "{~}", with: character)
                let markdownItem = sut.createMarkDownItemWithLines([string])
                XCTAssertEqual((markdownItem as! ItalicMarkDownItem).content, content)
            }
        }
    }

    func testGetAllMatches() {
        //Arrange
        let templateValue: [String: [NSRange]] = [
            "{~}Text{~}": [NSRange(location: 0, length: 6)],
            "{~}{~}Text that is bold{~}{~}": [],
            "{~}Text{~} test {~}Text{~}": [NSRange(location: 0, length: 6), NSRange(location: 12, length: 6)],
            "{~}Caption: \\{~}250ml cup/\\{~}\\{~}10-inch plate{~}": [NSRange(location: 0, length: 40)]
        ]
        //Act

        //Assert
        for (template, value) in templateValue {
            for character in italicCharacters {
                let string = template.replacingOccurrences(of: "{~}", with: character)
                XCTAssertEqual(sut.getAllMatches([string]), value)
            }
        }
    }
}
